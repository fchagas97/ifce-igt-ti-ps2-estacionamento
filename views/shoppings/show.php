<h1 class="h5 m-0 p-4 bg-light border-bottom">
    <i class="fa fa-chevron-circle-right d-inline-block mr-2"></i> Visualizar Shoppings
</h1>
<div class="p-4"><div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thead-light">
            <tr>
                <th>Nome</th>
                <th style="width:100px">Endereço</th>
                <th>Vagas</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($shoppings as $shopping): ?>
            <tr>
            <td class="align-middle text-truncate" title="<?php echo $shopping->nome; ?>"><?php echo $shopping->nome; ?></td>
            <td class="align-middle text-truncate"><?php echo $shopping->endereco; ?></td>
            <td class="align-middle text-center"><?php echo $totalVagas($shopping->id); ?></td>
            <td class="align-middle text-center">
                <a
                href="index.php?controller=vagas&action=show&shopping_id=<?php echo $shopping->id; ?>"
                class="badge badge-pill badge-info p-2">Ver vagas</a>
                <a
                href="#"
                data-href="index.php?controller=shoppings&action=delete&shopping_id=<?php echo $shopping->id; ?>"
                class="action_delete badge badge-pill badge-danger p-2"
                data-toggle="modal"
                data-target="#confirm-delete">Apagar</a>
                <a
                href="index.php?controller=shoppings&action=update&shopping_id=<?php echo $shopping->id; ?>"
                class="badge badge-pill badge-secondary p-2">Atualizar</a>
            </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
</div></div>
